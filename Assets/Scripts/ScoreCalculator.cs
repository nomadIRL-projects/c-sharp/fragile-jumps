using UnityEngine;
using UnityEngine.UI;

public class ScoreCalculator : MonoBehaviour
{
    private int _score;

    public Text scoreText;

    // Update is called once per frame
    public void Update()
    {
        scoreText.text = _score.ToString();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Score"))
        {
            ++_score;
        }
    }
}
