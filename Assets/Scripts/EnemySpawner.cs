using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyGroup;
    
    /**
     * How often enemies should spawn
     */
    public int spawnRate;

    /**
     * How far from the edge of the screen enemies should spawn
     */
    private const int SpawnDistance = 2;

    public void Start()
    {
        InvokeRepeating(nameof(SpawnEnemies), spawnRate, spawnRate);
    }

    protected void SpawnEnemies()
    {
        var verticalPosition = Random.Range(-6f, 5.5f);
        var enemies = Instantiate(
            enemyGroup, 
            new Vector3(SpawnDistance, verticalPosition, 0), 
            Quaternion.identity
        );
        
        Destroy(enemies, 10);
    }
}
