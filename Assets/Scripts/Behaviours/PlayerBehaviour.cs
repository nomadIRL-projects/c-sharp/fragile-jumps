using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    public float force;
    
    public Rigidbody2D playerBody;

    private Animator _animator;

    [SerializeField] private GameObject[] gameOverCards;
    
    // Start is called before the first frame update
    private void Start()
    {
        Time.timeScale = 1;
        
        playerBody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            playerBody.velocity = Vector2.up * force;
            SoundManager.PlaySound("jump");
        }

        if (playerBody.velocity.y < 0)
        {
            _animator.SetBool("isFalling", true);
            _animator.SetBool("isJumping", false);
        }
        else
        {
            _animator.SetBool("isJumping", true);
            _animator.SetBool("isFalling", false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            Destroy(gameObject);
            
            Time.timeScale = 0;
            
            ShowRestartText();
        }
    }

    private void ShowRestartText()
    {
        var concreteTextCardIndex = Random.Range(0, gameOverCards.Length);
        var concreteTextCard = gameOverCards[concreteTextCardIndex];
        
        concreteTextCard.SetActive(true);
        
        SoundManager.PlaySound("gameOver");
    }
}
