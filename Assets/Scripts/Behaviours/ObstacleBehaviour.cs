using UnityEngine;

public class ObstacleBehaviour : MonoBehaviour
{
    public float speed;

    // Update is called once per frame
    public void Update()
    {
        transform.Translate(-speed * Time.deltaTime, 0, 0);
    }
}
