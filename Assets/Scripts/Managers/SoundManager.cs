using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static AudioClip _jumpSound, _gameOverSound;
    private static AudioSource _audioSrc;

    public void Start()
    {
        _jumpSound = Resources.Load<AudioClip>("jump_sound");
        _gameOverSound = Resources.Load<AudioClip>("game_over");

        _audioSrc = GetComponent<AudioSource>();
    }
    

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "jump":
                _audioSrc.PlayOneShot(_jumpSound);
                break;
            case "gameOver":
                _audioSrc.PlayOneShot(_gameOverSound);
                break;
        }
    }
}
